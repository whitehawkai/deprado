import pandas as pd
import numpy as np

## SNIPPET 5.1 WEIGHTING FUNCTION
def getWeights(d,size): # thres>0 drops insignificant weights 
    w=[1.] for k in range(1,size): 
        w_=-w[-1]/k*(d-k+1) 
        w.append(w_) 
    w=np.array(w[::-1]).reshape(-1,1) 
    return w 

def plotWeights(dRange,nPlots,size): 
    w=pd.DataFrame() 
    for d in np.linspace(dRange[0],dRange[1],nPlots): 
        w_=getWeights(d,size=size) 
        w_=pd.DataFrame(w_,index=range(w_.shape[0])[::-1],columns=[d]) 
        w=w.join(w_,how='outer') 
    ax=w.plot() 
    ax.legend(loc='upper left');mpl.show() 
    return 

if __name__=='__main__': 
    plotWeights(dRange=[0,1],nPlots=11,size=6) 
    plotWeights(dRange=[1,2],nPlots=11,size=6)

## SNIPPET 5.2 STANDARD FRACDIFF (EXPANDING WINDOW) 
    # Increasing width window, with treatment of NaNs 
    # Note 1: For thres=1, nothing is skipped. 
    # Note 2: d can be any positive fractional, not necessarily bounded [0,1].
def fracDiff(series,d,thres=.01): 
    #1) Compute weights for the longest series 
    w=getWeights(d,series.shape[0]) 
    #2) Determine initial calcs to be skipped based on weight-loss threshold 
    w_=np.cumsum(abs(w)) 
    w_/=w_[-1] 
    skip=w_[w_>thres].shape[0] 
    #3) Apply weights to values 
    df={} 
    for name in series.columns: 
        seriesF,df_=series[[name]].ﬁllna(method='fﬁll').dropna(),pd.Series() 
        for iloc in range(skip,seriesF.shape[0]): 
            loc=seriesF.index[iloc] 
            if not np.isﬁnite(series.loc[loc,name]):continue # exclude NAs 
            df_[loc]=np.dot(w[-(iloc+1):,:].T,seriesF.loc[:loc])[0,0] 
        df[name]=df_.copy(deep=True) 
    df=pd.concat(df,axis=1) 
    return df

## SNIPPET 5.3 THE NEW FIXED-WIDTH WINDOW FRACDIFF METHOD
    # Constant width window (new solution) 
    # Note 1: thres determines the cut-off weight for the window 
    # Note 2: d can be any positive fractional, not necessarily bounded [0,1]. 
def fracDiff_FFD(series,d,thres=1e-5): 
    #1) Compute weights for the longest series 
    w=getWeights_FFD(d,thres) width=len(w)-1 
    #2) Apply weights to values
    df={}
    for name in series.columns: 
        seriesF,df_=series[[name]].ﬁllna(method='fﬁll').dropna(),pd.Series() 
        for iloc1 in range(width,seriesF.shape[0]): 
            loc0,loc1=seriesF.index[iloc1-width],seriesF.index[iloc1] 
            if not np.isﬁnite(series.loc[loc1,name]):continue  # exclude NAs 
            df_[loc1]=np.dot(w.T,seriesF.loc[loc0:loc1])[0,0] 
        df[name]=df_.copy(deep=True) 
    df=pd.concat(df,axis=1) 
    return df


## SNIPPET 5.4 FINDING THE MINIMUM D VALUE THAT PASSES THE ADF TEST
def plotMinFFD(): 
    from statsmodels.tsa.stattools 
    import adfuller path,instName='./','ES1_Index_Method12' 
    out=pd.DataFrame(columns=['adfStat','pVal','lags','nObs','95% conf','corr']) 
    df0=pd.read_csv(path+instName+'.csv',index_col=0,parse_dates=True) 
    for d in np.linspace(0,1,11): 
        df1=np.log(df0[['Close']]).resample('1D').last() # downcast to daily obs 
        df2=fracDiff_FFD(df1,d,thres=.01) 
        corr=np.corrcoef(df1.loc[df2.index,'Close'],df2['Close'])[0,1] 
        df2=adfuller(df2['Close'],maxlag=1,regression='c',autolag=None) 
        out.loc[d]=list(df2[:4])+[df2[4]['5%']]+[corr] # with critical value 
    out.to_csv(path+instName+'_testMinFFD.csv') 
    out[['adfStat','corr']].plot(secondary_y='adfStat') 
    mpl.axhline(out['95% conf'].mean(),linewidth=1,color='r',linestyle='dotted') 
    mpl.saveﬁg(path+instName+'_testMinFFD.png')
    return

