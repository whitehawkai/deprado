import pandas as pd
import numpy as numpy
import multiprocessing as mp
import copy_reg, types
import MultiCPU as mCPU

## SNIPPET 20.14 PRINCIPAL COMPONENTS FOR A SUBSET OF THE COLUMNS 
pcs=mpJobList(getPCs,('molecules',ﬁleNames),numThreads=24,mpBatches=1, path=path,eVec=eVec, \
        redux=pd.DataFrame.add) 
#—————————————————————————————————————— 
def getPCs(path,molecules,eVec): 
    # get principal components by loading one file at a time 
    pcs=None 
    for i in molecules: 
        df0=pd.read_csv(path+i,index_col=0,parse_dates=True) 
        if pcs is None:pcs=np.dot(df0.values,eVec.loc[df0.columns].values) 
        else:pcs+=np.dot(df0.values,eVec.loc[df0.columns].values) 
    pcs=pd.DataFrame(pcs,index=df0.index,columns=eVec.columns) 
    return pcs