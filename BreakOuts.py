import pandas as pd, numpy as np

#------------------------------------------------------#
##-------------- SYMMETRIC CUSUM FILTER ---------------##

## SNIPPET 2.4 THE SYMMETRIC CUSUM FILTER
def getTEvents(gRaw,h): 
    tEvents,sPos,sNeg=[],0,0
    diff=gRaw.diff()
    for i in diff.index[1:]: 
        sPos,sNeg=max(0,sPos+diff.loc[i]),min(0,sNeg+diff.loc[i]) 
        if sNeg<-h: 
            sNeg=0;tEvents.append(i) 
        elif sPos>h: 
            sPos=0;tEvents.append(i) 
    return pd.DatetimeIndex(tEvents)


#-------------------------------------------------------#
##------ SUPREMEUM AUGMENTED DICKEY FULLER TEST -------##

## SNIPPET 17.1 SADF’S INNER LOOP
def get_bsadf(logP,minSL,constant,lags): 
    y,x=getYX(logP,constant=constant,lags=lags) 
    startPoints,bsadf,allADF=range(0,y.shape[0]+lags-minSL+1),None,[] 
    for start in startPoints: 
        y_,x_=y[start:],x[start:] 
        bMean_,bStd_=getBetas(y_,x_) 
        bMean_,bStd_=bMean_[0,0],bStd_[0,0]**.5 
        allADF.append(bMean_/bStd_) 
        if allADF[-1]>bsadf:bsadf=allADF[-1] 
    out={'Time':logP.index[-1],'gsadf':bsadf} 
    return out

# Snippet 17.2 lists function getXY, which prepares the numpy objects needed to 
# conduct the recursive tests.
## SNIPPET 17.2 PREPARING THE DATASETS
def getYX(series,constant,lags): 
    series_=series.diff().dropna() 
    x=lagDF(series_,lags).dropna() 
    x.iloc[:,0]=series.values[-x.shape[0]-1:-1,0] # lagged level 
    y=series_.iloc[-x.shape[0]:].values
    if constant!='nc': 
        x=np.append(x,np.ones((x.shape[0],1)),axis=1) 
        if constant[:2]=='ct': 
            trend=np.arange(x.shape[0]).reshape(-1,1) 
            x=np.append(x,trend,axis=1) 
        if constant=='ctt': 
            x=np.append(x,trend**2,axis=1) 
    return y,x

# Snippet 17.3 lists function lagDF, 
# which applies to a dataframe the lags specified in its argument lags.
## SNIPPET 17.3 APPLY LAGS TO DATAFRAME
def lagDF(df0,lags): 
    df1=pd.DataFrame() 
    if isinstance(lags,int):lags=range(lags+1) 
    else:lags=[int(lag) for lag in lags] 
    for lag in lags: 
        df_=df0.shift(lag).copy(deep=True) 
        df_.columns=[str(i)+'_'+str(lag) for i in df_.columns] 
        df1=df1.join(df_,how='outer') 
    return df1

# Finally, Snippet 17.4 lists function getBetas, which carries out the actual regressions.
## SNIPPET 17.4 FITTING THE ADF SPECIFICATION
def getBetas(y,x): 
    xy=np.dot(x.T,y) 
    xx=np.dot(x.T,x) 
    xxinv=np.linalg.inv(xx) 
    bMean=np.dot(xxinv,xy) 
    err=y-np.dot(x,bMean) 
    bVar=np.dot(err.T,err)/(x.shape[0]-x.shape[1])*xxinv 
    return bMean,bVar