import pandas as pd, numpy as np 

## SNIPPET 3.1 DAILY VOLATILITY ESTIMATES
def getDailyVol(close,span0=100): 
    # daily vol, reindexed to close 
    df0=close.index.searchsorted(close.index-pd.Timedelta(days=1)) 
    df0=df0[df0>0] 
    df0=pd.Series(close.index[df0–1], index=close.index[close.shape[0]-df0.shape[0]:]) 
    df0=close.loc[df0.index]/close.loc[df0.values].values-1 # daily returns 
    df0=df0.ewm(span=span0).std() 
    return df0

## SNIPPET 4.11 IMPLEMENTATION OF TIME-DECAY FACTORS
# Snippet 4.11 implements this form of time-decay factors. 
# Note that time is not meant to be chronological. 
# In this implementation, decay takes place according to cumulative uniqueness, 
# x ∈[0,∑I i=1 ̄ ui], because a chronological decay would reduce weights
#  too fast in the presence of redundant observations.
 def getLinearTimeDecay(tW,clfLastW=1.): 
    # apply piecewise-linear decay to observed uniqueness (tW) 
    # newest observation gets weight=1, 
    # oldest observation gets weight=clfLastW 
    clfW=tW.sort_index().cumsum() 
    if clfLastW>=0:slope=(1.-clfLastW)/clfW.iloc[-1] 
    else:slope=1./((clfLastW+1)*clfW.iloc[-1]) 
    const=1.-slope*clfW.iloc[-1] 
    clfW=const+slope*clfW 
    clfW[clfW<0]=0 
    print const,slope 
    return clfW

 def getExpTimeDecay(tW,decay=0., decayType='perU'): 
    # apply piecewise-exponential decay to observed uniqueness (tW) 
    # newest observation gets weight = 1, 
    # if decayType = 'perU' then
        # oldest observation gets weight = exp(decay*(numUniqueness before end))
    # else if decayType = 'toEnd' then  
        # oldest observation gets weight = decay
    clfW=tW.sort_index().cumsum()
    if decayType.lower() == 'toend': decay = -np.log(decay) * clfW[-1]
    clfW=exp(decay*(clfW[-1]-clfW)) 
    clfW[clfW<0]=0 
    print const,slope 
    return clfW
