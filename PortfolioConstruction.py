import numpy as np
import pandas as pd
import scipy.cluster.hierarchy as sch
import matplotlib.pyplot as mpl


#------------------------------------------------#
##---  PRINCIPAL COMPONENTS WEIGHTING SCHEME ---##

## Snippet 2.1 PCA WEIGHTS FROM A RISK DISTRIBUTION R
def pcaWeights(cov,riskDist=None,riskTarget=1.): 
    # Following the riskAlloc distribution, match riskTarget 
    eVal,eVec=np.linalg.eigh(cov) # must be Hermitian 
    indices=eVal.argsort()[::-1] # arguments for sorting eVal desc 
    eVal,eVec=eVal[indices],eVec[:,indices]
    if riskDist is None: 
        riskDist=np.zeros(cov.shape[0]) 
        riskDist[-1]=1. 
    loads=riskTarget*(riskDist/eVal)**.5 
    wghts=np.dot(eVec,np.reshape(loads,(-1,1))) 
    #ctr=(loads/riskTarget)**2*eVal # verify riskDist 
    return wghts


#----------------------------------------#
##------  HIERARCHICAL RISK PARITY -----##

##SNIPPET 16.1 TREE CLUSTERING USING SCIPY FUNCTIONALITY
# Define a linkage matrix as an (N −1)x4 matrix with structure Y ={ (ym,1,ym,2,ym,3,ym,4)}m=1,…,N−1
# (i.e., with one 4-tuple per cluster). 
# Items (ym,1,ym,2) report the constituents. 
# Item ym,3 reports the distance between ym,1 and ym,2, that is ym,3 = ̃ dym,1,ym,2. 
# Item ym,4 ≤ N reports the number of original items included in cluster m.
cov,corr=x.cov(),x.corr() 
dist=((1-corr)/2.)**.5 # distance matrix 
link=sch.linkage(dist,'single') # linkage matrix

## SNIPPET 16.2 QUASI-DIAGONALIZATION
def getQuasiDiag(link): 
    # Sort clustered items by distance 
    link=link.astype(int) 
    sortIx=pd.Series([link[-1,0],link[-1,1]]) 
    numItems=link[-1,3] # number of original items 
    while sortIx.max()>=numItems: 
        sortIx.index=range(0,sortIx.shape[0]*2,2) # make space 
        df0=sortIx[sortIx>=numItems] # find clusters 
        i=df0.index;j=df0.values-numItems 
        sortIx[i]=link[j,0] # item 1 
        df0=pd.Series(link[j,1],index=i+1) 
        sortIx=sortIx.append(df0) # item 2 
        sortIx=sortIx.sort_index() # re-sort 
        sortIx.index=range(sortIx.shape[0]) # re-index 
    return sortIx.tolist()

## SNIPPET 16.3 RECURSIVE BISECTION
def getRecBipart(cov,sortIx): 
    # Compute HRP alloc 
    w=pd.Series(1,index=sortIx) 
    cItems=[sortIx] # initialize all items in one cluster 
    while len(cItems)>0: 
        cItems=[i[j:k] for i in cItems for j,k in ((0,len(i)/2),\ 
            (len(i)/2,len(i))) if len(i)>1] # bi-section 
        for i in xrange(0,len(cItems),2): # parse in pairs 
            cItems0=cItems[i] # cluster 1 
            cItems1=cItems[i+1] # cluster 2 
            cVar0=getClusterVar(cov,cItems0) 
            cVar1=getClusterVar(cov,cItems1) 
            alpha=1-cVar0/(cVar0+cVar1) 
            w[cItems0]*=alpha # weight 1 
            w[cItems1]*=1-alpha # weight 2 
    return w


## SNIPPET 16.4 FULL IMPLEMENTATION OF THE HRP ALGORITHM
# Snippet 16.4 can be used to reproduce our results and simulate additional numerical examples.
# FunctiongenerateDataproducesamatrixoftimeserieswhereanumber size0 of vectors are uncorrelated, 
# and a number size1 of vectors are correlated. The reader can change the np.random.seed in 
# generateData to run alternative examplesandgainanintuitionofhowHRPworks.
# Scipy’sfunctionlinkagecanbe used to perform stage 1 (Section 16.4.1), 
# function getQuasiDiag performs stage 2 (Section 16.4.2), 
# and function getRecBipart carries out stage 3 (Section 16.4.3).
#——————————————————————————————————————— 
def getIVP(cov,**kargs): # Compute the inverse-variance portfolio
    ivp=1./np.diag(cov)
    ivp/=ivp.sum() 
return ivp 
#——————————————————————————————————————— 
def getClusterVar(cov,cItems): # Compute variance per cluster 
    cov_=cov.loc[cItems,cItems] # matrix slice 
    w_=getIVP(cov_).reshape(-1,1) 
    cVar=np.dot(np.dot(w_.T,cov_),w_)[0,0] 
    return cVar 
#——————————————————————————————————————— 
def getQuasiDiag(link): # Sort clustered items by distance 
    link=link.astype(int) 
    sortIx=pd.Series([link[-1,0],link[-1,1]]) 
    numItems=link[-1,3] # number of original items 
    while sortIx.max()>=numItems: 
        sortIx.index=range(0,sortIx.shape[0]*2,2) # make space 
        df0=sortIx[sortIx>=numItems] # find clusters 
        i=df0.index;j=df0.values-numItems 
        sortIx[i]=link[j,0] # item 1
        df0=pd.Series(link[j,1],index=i+1) 
        sortIx=sortIx.append(df0) # item 2 
        sortIx=sortIx.sort_index() # re-sort 
        sortIx.index=range(sortIx.shape[0]) # re-index 
    return sortIx.tolist() 
#——————————————————————————————————————— 
def getRecBipart(cov,sortIx): 
    # Compute HRP alloc 
    w=pd.Series(1,index=sortIx) 
    cItems=[sortIx] # initialize all items in one cluster 
    while len(cItems)>0: 
        cItems=[i[j:k] for i in cItems for j,k in ((0,len(i)/2), \ 
            (len(i)/2,len(i))) if len(i)>1] # bi-section 
        for i in xrange(0,len(cItems),2): # parse in pairs 
            cItems0=cItems[i] # cluster 1 
            cItems1=cItems[i+1] # cluster 2 
            cVar0=getClusterVar(cov,cItems0) 
            cVar1=getClusterVar(cov,cItems1) 
            alpha=1-cVar0/(cVar0+cVar1) 
            w[cItems0]*=alpha # weight 1 
            w[cItems1]*=1-alpha # weight 2 return w 
#——————————————————————————————————————— 
def correlDist(corr): # A distance matrix based on correlation, where 0<=d[i,j]<=1 
# This is a proper distance metric 
    dist=((1-corr)/2.)**.5 # distance matrix 
    return dist 
#——————————————————————————————————————— 
def plotCorrMatrix(path,corr,labels=None): # Heatmap of the correlation matrix 
    if labels is None:labels=[] 
    mpl.pcolor(corr) 
    mpl.colorbar() 
    mpl.yticks(np.arange(.5,corr.shape[0]+.5),labels) 
    mpl.xticks(np.arange(.5,corr.shape[0]+.5),labels) 
    mpl.saveﬁg(path) mpl.clf();mpl.close() # reset pylab 
    return 
#——————————————————————————————————————— 
def generateData(nObs,size0,size1,sigma1): # Time series of correlated variables 
    #1) generating some uncorrelated data 
    np.random.seed(seed=12345);random.seed(12345) 
    x=np.random.normal(0,1,size=(nObs,size0)) # each row is a variable 
    #2) creating correlation between the variables 
    cols=[random.randint(0,size0–1) for i in xrange(size1)] 
    y=x[:,cols]+np.random.normal(0,sigma1,size=(nObs,len(cols))) 
    x=np.append(x,y,axis=1)
    x=pd.DataFrame(x,columns=range(1,x.shape[1]+1)) 
    return x,cols 
#——————————————————————————————————————— 
def main(): 
    #1) Generate correlated data 
    nObs,size0,size1,sigma1=10000,5,5,.25 
    x,cols=generateData(nObs,size0,size1,sigma1) 
    print [(j+1,size0+i) for i,j in enumerate(cols,1)] 
    cov,corr=x.cov(),x.corr() 
    #2) compute and plot correl matrix 
    plotCorrMatrix('HRP3_corr0.png',corr,labels=corr.columns) 
    #3) cluster dist=correlDist(corr) 
    link=sch.linkage(dist,'single') sortIx=getQuasiDiag(link) 
    sortIx=corr.index[sortIx].tolist() # recover labels 
    df0=corr.loc[sortIx,sortIx] # reorder 
    plotCorrMatrix('HRP3_corr1.png',df0,labels=df0.columns) 
    #4) Capital allocation 
    hrp=getRecBipart(cov,sortIx) 
    print hrp 
    return 
    #——————————————————————————————————————— 
    if __name__=='__main__':main()


