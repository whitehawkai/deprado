import numpy as np
import pandas as pd
import multiprocessing as mp


##SNIPPET 3.2 TRIPLE-BARRIER LABELING METHOD
# Snippet 3.2 implements the triple-barrier method. 
# The function receives four arguments:
#  close: A pandas series of prices. 
#  events: A pandas dataframe, with columns, 
#     ◦ t1: The timestamp of vertical barrier. 
#         When the value is np.nan, there will not be a vertical barrier. 
#     ◦ trgt: The unit width of the horizontal barriers. 
#  ptSl: A list of two non-negative float values: 
#     ◦ ptSl[0]:The factor that multiplies trgt to set the width of the upper barrier. 
#         If 0, there will not be an upper barrier. 
#     ◦ ptSl[1]:The factor that multiplies trgt to set the width of the lower barrier. 
#         If 0, there will not be a lower barrier. 
#  molecule: A list with the subset of event indices that will be processed by a single thread. 
#     Its use will become clear later on in the chapter.

def applyPtSlOnT1(close,events,ptSl,molecule): 
    # apply stop loss/profit taking, if it takes place before t1 (end of event) 
    events_=events.loc[molecule] 
    out=events_[['t1']].copy(deep=True)
    if ptSl[0]>0:pt=ptSl[0]*events_['trgt']
    else:pt=pd.Series(index=events.index) # NaNs 
    if ptSl[1]>0:sl=-ptSl[1]*events_['trgt']
    else:sl=pd.Series(index=events.index) # NaNs 
    for loc,t1 in events_['t1'].ﬁllna(close.index[-1]).iteritems(): 
        df0=close[loc:t1] # path prices 
        df0=(df0/close[loc]-1)*events_.at[loc,'side'] # path returns 
        out.loc[loc,'sl']=df0[df0<sl[loc]].index.min() # earliest stop loss. 
        out.loc[loc,'pt']=df0[df0>pt[loc]].index.min() # earliest profit taking. 
    return out

## SNIPPET 3.3 GETTING THE TIME OF FIRST TOUCH 
# Snippet 3.3 implements the function getEvents, which finds the time of the first barrier touch. 
# The function receives the following arguments:
#  close: A pandas series of prices. 
#  tEvents:Thepandastimeindexcontainingthetimestampsthatwillseedevery triple barrier. 
#     These are the timestamps selected by the sampling procedures discussed in Chapter 2, Section 2.5. 
#  ptSl: A non-negative float that sets the width of the two barriers. 
#     A 0 value means that the respective horizontal barrier 
#     (profit taking and/or stop loss) will be disabled. 
#  t1: A pandas series with the timestamps of the vertical barriers. 
#     We pass a False when we want to disable vertical barriers. 
#  trgt: A pandas series of targets, expressed in terms of absolute returns. 
#  minRet:Theminimumtargetreturnrequiredforrunningatriplebarriersearch. 
#  numThreads: The number of threads concurrently used by the function.
def getEvents(close,tEvents,ptSl,trgt,minRet,numThreads,t1=False): 
    #1) get target 
    trgt=trgt.loc[tEvents] 
    trgt=trgt[trgt>minRet] # minRet 
    #2) get t1 (max holding period) 
    if t1 is False:t1=pd.Series(pd.NaT,index=tEvents) 
    #3) form events object, apply stop loss on t1
    side_=pd.Series(1.,index=trgt.index) 
    events=pd.concat({'t1':t1,'trgt':trgt,'side':side_}, \ 
        axis=1).dropna(subset=['trgt']) 
    df0=mpPandasObj(func=applyPtSlOnT1,pdObj=('molecule',events.index), \ 
        numThreads=numThreads,close=close,events=events,ptSl=[ptSl,ptSl]) 
    events['t1']=df0.dropna(how='all').min(axis=1) # pd.min ignores nan 
    events=events.drop('side',axis=1) 
    return events

## SNIPPET 3.4 ADDING A VERTICAL BARRIER
t1=close.index.searchsorted(tEvents+pd.Timedelta(days=numDays)) 
t1=t1[t1<close.shape[0]] 
t1=pd.Series(close.index[t1],index=tEvents[:t1.shape[0]]) # NaNs at end

## SNIPPET 3.5 LABELING FOR SIDE AND SIZE
def getBins(events,close): 
    #1) prices aligned with events 
    events_=events.dropna(subset=['t1']) 
    px=events_.index.union(events_['t1'].values).drop_duplicates() 
    px=close.reindex(px,method='bﬁll')
    #2) create out object
    out=pd.DataFrame(index=events_.index) 
    out['ret']=px.loc[events_['t1'].values].values/px.loc[events_.index]-1 
    out['bin']=np.sign(out['ret']) 
    return out

## SNIPPET 3.6 EXPANDING getEvents TO INCORPORATE META-LABELING 
def getEvents(close,tEvents,ptSl,trgt,minRet,numThreads,t1=False,side=None): 
    #1) get target 
    trgt=trgt.loc[tEvents] 
    trgt=trgt[trgt>minRet] # minRet 
    #2) get t1 (max holding period) 
    if t1 is False:t1=pd.Series(pd.NaT,index=tEvents) 
    #3) form events object, apply stop loss on t1 
    if side is None:side_,ptSl_=pd.Series(1.,index=trgt.index),[ptSl[0],ptSl[0]] 
    else:side_,ptSl_=side.loc[trgt.index],ptSl[:2] 
    events=pd.concat({'t1':t1,'trgt':trgt,'side':side_}, \ 
        axis=1).dropna(subset=['trgt']) 
    df0=mpPandasObj(func=applyPtSlOnT1,pdObj=('molecule',events.index), \ 
        numThreads=numThreads,close=inst['Close'],events=events,ptSl=ptSl_)
    events['t1']=df0.dropna(how='all').min(axis=1) # pd.min ignores nan 
    if side is None:events=events.drop('side',axis=1) 
    return events

## SNIPPET 3.7 EXPANDING getBins TO INCORPORATE META-LABELING
def getBins(events,close): 
    # ’’’ 
    # Compute event's outcome (including side information, if provided). 
    # events is a DataFrame where: 
    #     —events.index is event's starttime 
    #     —events[’t1’] is event's endtime 
    #     —events[’trgt’] is event's target 
    #     —events[’side’] (optional) implies the algo's position side 
    # Case 1: (’side’ not in events): bin in (-1,1) <—label by price action 
    # Case 2: (’side’ in events): bin in (0,1) <—label by pnl (meta-labeling) 
    # ’’’
    #1) prices aligned with events 
    events_=events.dropna(subset=['t1']) 
    px=events_.index.union(events_['t1'].values).drop_duplicates() 
    px=close.reindex(px,method='bﬁll') 
    #2) create out object 
    out=pd.DataFrame(index=events_.index) 
    out['ret']=px.loc[events_['t1'].values].values/px.loc[events_.index]-1 
    if 'side' in events_:out['ret']*=events_['side'] # meta-labeling 
    out['bin']=np.sign(out['ret']) 
    if 'side' in events_:out.loc[out['ret']<=0,'bin']=0 # meta-labeling 
    return out

## SNIPPET 3.8 DROPPING UNDER-POPULATED LABELS
# Some ML classifiers do not perform well when classes are too imbalanced. 
# In those circumstances, it is preferable to drop extremely rare labels 
# and focus on the more common outcomes. 
# Snippet 3.8 presents a procedure that recursively drops observations 
# associated with extremely rare labels. 
# Function dropLabels recursively eliminates those observations associated 
# with classes that appear less than a fraction minPct of cases, 
#unless there are only two classes left.
def dropLabels(events,minPtc=.05): # apply weights, drop labels with insufficient examples 
    while True: 
        df0=events['bin'].value_counts(normalize=True) 
        if df0.min()>minPct or df0.shape[0]<3:break 
        print 'dropped label',df0.argmin(),df0.min() 
        events=events[events['bin']!=df0.argmin()] 
    return events

## SNIPPET 20.4 MULTIPROCESSING IMPLEMENTATION OF A ONE-TOUCH DOUBLE BARRIER
def main1(): # Path dependency: Multi-threaded implementation 
    r,numThreads=np.random.normal(0,.01,size=(1000,10000)),24 
    parts=np.linspace(0,r.shape[0],min(numThreads,r.shape[0])+1) 
    parts,jobs=np.ceil(parts).astype(int),[] 
    for i in xrange(1,len(parts)): 
        jobs.append(r[:,parts[i-1]:parts[i]]) # parallel jobs
    pool,out=mp.Pool(processes=numThreads),[]
    outputs=pool.imap_unordered(barrierTouch,jobs) 
    for out_ in outputs:out.append(out_) # asynchronous response 
    pool.close();pool.join() 
    return 
#——————————————————————————————————————— 
if __name__=='__main__': 
    import timeit 
    print min(timeit.Timer('main1()',setup='from __main__ import main1').repeat(5,10))