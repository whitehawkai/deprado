import pandas as pd
import numpy as numpy
import multiprocessing as mp
import copy_reg, types

## SNIPPET 20.11 PICKLE METHOD - PLACE THIS CODE AT THE BEGINNING OF YOUR ENGINE
def _pickle_method(method): 
    func_name=method.im_func.__name__ 
    obj=method.im_self 
    cls=method.im_class 
    return _unpickle_method,(func_name,obj,cls) 
#——————————————————————————————————————— 
def _unpickle_method(func_name,obj,cls): 
    for cls in cls.mro(): 
        try:func=cls.__dict__[func_name] 
        except KeyError:pass 
        else:break 
    return func.__get__(obj,cls) 
#——————————————————————————————————————— 
import copy_reg,types,multiprocessing as mp 
copy_reg.pickle(types.MethodType,_pickle_method,_unpickle_method)


## SNIPPET 20.5 THE linParts FUNCTION
#——————————————————————————————————————— 
def linParts(numAtoms,numThreads): # partition of atoms with a single loop 
    parts=np.linspace(0,numAtoms,min(numThreads,numAtoms)+1) 
    parts=np.ceil(parts).astype(int) 
    return parts


## SNIPPET 20.6 THE nestedParts FUNCTION
def nestedParts(numAtoms,numThreads,upperTriang=False): # partition of atoms with an inner loop 
    parts,numThreads_=[0],min(numThreads,numAtoms) 
    for num in xrange(numThreads_): 
        part=1+4*(parts[-1]**2+parts[-1]+numAtoms*(numAtoms+1.)/numThreads_) 
        part=(-1+part**.5)/2. 
        parts.append(part) 
    parts=np.round(parts).astype(int) 
    if upperTriang: # the first rows are the heaviest 
        parts=np.cumsum(np.diff(parts)[::-1]) 
        parts=np.append(np.array([0]),parts) 
    return parts

# sNIPPET 20.7 mpPandasObj FUNCTION
# mpPandasObj receives six arguments, of which four are optional:
#  func: A callback function, which will be executed in parallel 
#  pdObj: A tuple containing: 
#     ◦ The name of the argument used to pass molecules to the callback function 
#     ◦ A list of indivisible tasks (atoms), which will be grouped into molecules 
#  numThreads:Thenumberofthreadsthatwillbeusedinparallel(oneprocessor per thread) 
#  mpBatches: Number of parallel batches (jobs per core) 
#  linMols: Whether partitions will be linear or double-nested 
#  kargs: Keyword arguments needed by func
#     Parallelize jobs, return a DataFrame or Series 
#     + func: function to be parallelized. Returns a DataFrame 
#     + pdObj[0]: Name of argument used to pass the molecule 
#     + pdObj[1]: List of atoms that will be grouped into molecules 
#     + kargs: any other argument needed by func
#     Example: df1=mpPandasObj(func,(’molecule’,df0.index),24,**kargs)
def mpPandasObj(func,pdObj,numThreads=24,mpBatches=1,linMols=True,**kargs): 
    if linMols:parts=linParts(len(argList[1]),numThreads*mpBatches) 
    else:parts=nestedParts(len(argList[1]),numThreads*mpBatches) 
    jobs=[] 
    for i in xrange(1,len(parts)): 
        job={pdObj[0]:pdObj[1][parts[i-1]:parts[i]],'func':func} 
        job.update(kargs) 
        jobs.append(job) 
    if numThreads==1:out=processJobs_(jobs) 
    else:out=processJobs(jobs,numThreads=numThreads) 
    if isinstance(out[0],pd.DataFrame):df0=pd.DataFrame() 
    elif isinstance(out[0],pd.Series):df0=pd.Series() 
    else:return out 
    for i in out:df0=df0.append(i) 
    df0=df0.sort_index() 
    return df0
    
## SNIPPET 20.8 SINGLE-THREAD EXECUTION, FOR DEBUGGING
def processJobs_(jobs): # Run jobs sequentially, for debugging 
    out=[] 
    for job in jobs: 
        out_=expandCall(job) 
        out.append(out_) 
    return out

## SNIPPET 20.9 EXAMPLE OF ASYNCHRONOUS CALL TO PYTHON’S MULTIPROCESSING LIBRARY
#——————————————————————————————————————— 
def reportProgress(jobNum,numJobs,time0,task): 
    # Report progress as asynch jobs are completed 
    msg=[ﬂoat(jobNum)/numJobs,(time.time()-time0)/60.] 
    msg.append(msg[1]*(1/msg[0]-1)) 
    timeStamp=str(dt.datetime.fromtimestamp(time.time())) 
    msg=timeStamp+' '+str(round(msg[0]*100,2))+'% '+task+' done after '+ \ 
        str(round(msg[1],2))+' minutes. Remaining '+str(round(msg[2],2))+' minutes.' 
    if jobNum<numJobs:sys.stderr.write(msg+'\r') 
    else:sys.stderr.write(msg+'\n') 
    return 
#——————————————————————————————————————— 
def processJobs(jobs,task=None,numThreads=24): 
    # Run in parallel. 
    # jobs must contain a ’func’ callback, for expandCall 
    if task is None:task=jobs[0]['func'].__name__ 
    pool=mp.Pool(processes=numThreads) 
    outputs,out,time0=pool.imap_unordered(expandCall,jobs),[],time.time() 
    # Process asynchronous output, report progress 
    for i,out_ in enumerate(outputs,1): 
        out.append(out_) 
        reportProgress(i,len(jobs),time0,task) 
    pool.close();pool.join() # this is needed to prevent memory leaks 
    return out

# 20.5.3 Unwrapping the Callback
# In Snippet 20.9, the instruction pool.imap_unordered() parallelized expandCall, 
# by running each item in jobs (a molecule) in a single thread. 
# Snippet 20.10 lists expandCall, which unwraps the items (atoms) in the job (molecule), 
# and executesthecallbackfunction.
# Thislittlefunctionisthetrickatthecoreofthemultiprocessing engine: 
# It transforms a dictionary into a task. Once you understand the role it plays, 
# you will be able to develop your own engines.
# SNIPPET 20.10 PASSING THE JOB (MOLECULE) TO THE CALLBACK FUNCTION
def expandCall(kargs): # Expand the arguments of a callback function, kargs[’func’] 
    func=kargs['func'] 
    del kargs['func'] 
    out=func(**kargs) 
    return out

## SNIPPET 20.12 ENHANCING processJobs TO PERFORM ON-THE-FLY OUTPUT REDUCTION 
def processJobsRedux(jobs,task=None,numThreads=24,redux=None,reduxArgs={}, reduxInPlace=False): 
    ''' Run in parallel jobs must contain a ’func’ callback, for expandCall redux prevents 
    wasting memory by reducing output on the fly ''' 
    if task is None:task=jobs[0]['func'].__name__ 
    pool=mp.Pool(processes=numThreads) 
    imap,out,time0=pool.imap_unordered(expandCall,jobs),None,time.time() 
    # Process asynchronous output, report progress 
    for i,out_ in enumerate(imap,1): 
        if out is None: 
            if redux is None:out,redux,reduxInPlace=[out_],list.append,True 
            else:out=copy.deepcopy(out_) 
        else: 
            if reduxInPlace:redux(out,out_,**reduxArgs) 
            else:out=redux(out,out_,**reduxArgs) 
        reportProgress(i,len(jobs),time0,task) 
    pool.close();pool.join() # this is needed to prevent memory leaks 
    if isinstance(out,(pd.Series,pd.DataFrame)):out=out.sort_index() 
    return out

## SNIPPET 20.13 ENHANCING mpPandasObj TO PERFORM ON-THE-FLY OUTPUT REDUCTION 
def mpJobList(func,argList,numThreads=24,mpBatches=1,linMols=True,redux=None, \
        reduxArgs={},reduxInPlace=False,**kargs):
    if linMols:parts =linParts(len(argList[1]),numThreads*mpBatches) 
    else:parts=nestedParts(len(argList[1]),numThreads*mpBatches) 
    jobs=[] 
    for i in xrange(1,len(parts)): 
        job={argList[0]:argList[1][parts[i-1]:parts[i]],'func':func} 
        job.update(kargs) 
        jobs.append(job) 
    out=processJobsRedux(jobs,redux=redux,reduxArgs=reduxArgs, reduxInPlace=reduxInPlace, \
            numThreads=numThreads)
    return out