import pandas as pd, numpy as np 

#----------------------------------------#
##----  ROLLING FUTURES TIME SERIES ----##

## SNIPPET 2.2 FORM A GAPS SERIES, DETRACT IT FROM PRICES
#——— PART 1
def getRolledSeries(pathIn,key): 
    series=pd.read_hdf(pathIn,key='bars/ES_10k')
    series['Time']=pd.to_datetime(series['Time'],format='%Y%m%d%H%M%S%f') 
    series=series.set_index('Time') 
    gaps=rollGaps(series) 
    for ﬂd in ['Close','VWAP']:series[ﬂd]-=gaps 
    return series 
#——— PART 2
def rollGaps(series,dictio={'Instrument':'FUT_CUR_GEN_TICKER','Open':'PX_OPEN', \
    'Close':'PX_LAST'},matchEnd=True): 
    # Compute gaps at each roll, between previous close and next open 
    rollDates=series[dictio['Instrument']].drop_duplicates(keep='ﬁrst').index 
    gaps=series[dictio['Close']]*0 
    iloc=list(series.index) 
    iloc=[iloc.index(i)-1 for i in rollDates] # index of days prior to roll 
    gaps.loc[rollDates[1:]]=series[dictio['Open']].loc[rollDates[1:]]- \ 
        series[dictio['Close']].iloc[iloc[1:]].values 
        gaps=gaps.cumsum() 
    if matchEnd:gaps-=gaps.iloc[-1] # roll backward
    return gaps

## SNIPPET 2.3 NON-NEGATIVE ROLLED PRICE SERIES
def getNonNegRolledSeries(ﬁlePath): 
    raw=pd.read_csv(ﬁlePath,index_col=0,parse_dates=True) 
    gaps=rollGaps(raw,dictio={'Instrument':'Symbol','Open':'Open','Close':'Close'})
    rolled=raw.copy(deep=True) 
    for ﬂd in ['Open','Close']:rolled[ﬂd]-=gaps 
    rolled['Returns']=rolled['Close'].diff()/raw['Close'].shift(1) 
    rolled['rPrices']=(1+rolled['Returns']).cumprod()
    return rolled


#----------------------------------------#
##-------------  LABELLING -------------##

## SNIPPET 4.1 ESTIMATING THE UNIQUENESS OF A LABEL
# Compute the number of concurrent events per bar. 
# +molecule[0] is the date of the first event on which the weight will be computed 
# +molecule[-1] is the date of the last event on which the weight will be computed
# Any event that starts before t1[molecule].max() impacts the count. 
def mpNumCoEvents(closeIdx,t1,molecule): 
   #1) find events that span the period [molecule[0],molecule[-1]] 
   t1=t1.ﬁllna(closeIdx[-1]) # unclosed events still must impact other weights 
   t1=t1[t1>=molecule[0]] # events that end at or after molecule[0] 
   t1=t1.loc[:t1[molecule].max()] # events that start at or before t1[molecule].max() 
   #2) count events spanning a bar 
   iloc=closeIdx.searchsorted(np.array([t1.index[0],t1.max()])) 
   count=pd.Series(0,index=closeIdx[iloc[0]:iloc[1]+1]) 
   for tIn,tOut in t1.iteritems():count.loc[tIn:tOut]+=1. 
   return count.loc[molecule[0]:t1[molecule].max()]

## SNIPPET 4.2 ESTIMATING THE AVERAGE UNIQUENESS OF A LABEL
# Derive average uniqueness over the event's lifespan
def mpSampleTW(t1,numCoEvents,molecule): 
    wght=pd.Series(index=molecule)
    for tIn,tOut in t1.loc[wght.index].iteritems(): 
        wght.loc[tIn]=(1./numCoEvents.loc[tIn:tOut]).mean() 
    return wght 
#——————————————————————————————————————— 
numCoEvents=mpPandasObj(mpNumCoEvents,('molecule',events.index),numThreads, \ 
    closeIdx=close.index,t1=events['t1']) 
numCoEvents=numCoEvents.loc[~numCoEvents.index.duplicated(keep='last')] 
numCoEvents=numCoEvents.reindex(close.index).ﬁllna(0) 
out['tW']=mpPandasObj(mpSampleTW,('molecule',events.index),numThreads, \ 
    t1=events['t1'],numCoEvents=numCoEvents)

## SNIPPET 4.3 BUILD AN INDICATOR MATRIX
def getIndMatrix(barIx,t1):
    indM=pd.DataFrame(0,index=barIx,columns=range(t1.shape[0])) 
    for i,(t0,t1) in enumerate(t1.iteritems()):indM.loc[t0:t1,i]=1. 
    return indM

## SNIPPET 4.4 COMPUTE AVERAGE UNIQUENESS
# Average uniqueness from indicator matrix
def getAvgUniqueness(indM): 
    c=indM.sum(axis=1) # concurrency 
    u=indM.div(c,axis=0) # uniqueness 
    avgU=u[u>0].mean() # average uniqueness 
    return avgU

## SNIPPET 4.5 RETURN SAMPLE FROM SEQUENTIAL BOOTSTRAP 
# Snippet4.5givesustheindexofthefeaturessampledbysequentialbootstrap.
# The inputsaretheindicatormatrix(indM)andanoptionalsamplelength(sLength),
# with a default value of as many draws as rows in indM.
def seqBootstrap(indM,sLength=None): 
    if sLength is None:sLength=indM.shape[1] 
    phi=[] 
    while len(phi)<sLength: 
        avgU=pd.Series() 
        for i in indM: 
            indM_=indM[phi+[i]] # reduce indM 
            avgU.loc[i]=getAvgUniqueness(indM_).iloc[-1] 
        prob=avgU/avgU.sum() # draw prob 
        phi+=[np.random.choice(indM.columns,p=prob)] 
    return phi

## SNIPPET 4.6 EXAMPLE OF SEQUENTIAL BOOTSTRAP
def main(): 
    t1=pd.Series([2,3,5],index=[0,2,4]) # t0,t1 for each feature obs 
    barIx=range(t1.max()+1) # index of bars 
    indM=getIndMatrix(barIx,t1) 
    phi=np.random.choice(indM.columns,size=indM.shape[1]) 
    print phi 
    print 'Standard uniqueness:',getAvgUniqueness(indM[phi]).mean() 
    phi=seqBootstrap(indM) 
    print phi 
    print 'Sequential uniqueness:',getAvgUniqueness(indM[phi]).mean() 
    return

## SNIPPET 4.7 GENERATING A RANDOM T1 SERIES
# We can evaluate the efficiency of the sequential bootstrap algorithm through
# experimentalmethods. Snippet4.7liststhefunctionthatgeneratesarandom t1 
# seriesfora number of observations numObs (I). 
# Each observation is made at a random number, drawnfromauniformdistribution,
# withboundaries0andnumBars,wherenumBars is the number of bars (T). 
# The number of bars spanned by the observation is determined by drawing
# a random number from a uniform distribution with boundaries 0 and maxH.
def getRndT1(numObs,numBars,maxH): 
    t1=pd.Series()
    for i in xrange(numObs): 
        ix=np.random.randint(0,numBars) 
        val=ix+np.random.randint(1,maxH) 
        t1.loc[ix]=val 
    return t1.sort_index()

#----------------------------------------#
##------  K FOLD CROSS VALIDATION ------##

## SNIPPET 7.1 PURGING OBSERVATION IN THE TRAINING SET
# Given testTimes, find the times of the training observations. 
# —t1.index: Time when the observation started. 
# —t1.value: Time when the observation ended. 
# —testTimes: Times of testing observations.  
def getTrainTimes(t1,testTimes): ’’’ 
    trn=t1.copy(deep=True) 
    for i,j in testTimes.iteritems(): 
        df0=trn[(i<=trn.index)&(trn.index<=j)].index # train starts within test 
        df1=trn[(i<=trn)&(trn<=j)].index # train ends within test 
        df2=trn[(trn.index<=i)&(j<=trn)].index # train envelops test 
        trn=trn.drop(df0.union(df1).union(df2)) 
        return trn

## SNIPPET 7.2 EMBARGO ON TRAINING OBSERVATIONS
def getEmbargoTimes(times,pctEmbargo): 
    # Get embargo time for each bar 
    step=int(times.shape[0]*pctEmbargo) 
    if step==0: 
        mbrg=pd.Series(times,index=times) 
    else: 
        mbrg=pd.Series(times[step:],index=times[:-step]) 
        mbrg=mbrg.append(pd.Series(times[-1],index=times[-step:])) 
    return mbrg 
#——————————————————————————————————————— 
testTimes  = pd.Series(mbrg[dt1],index=[dt0]) # include embargo before purge 
trainTimes = getTrainTimes(t1,testTimes) 
testTimes  = t1.loc[dt0:dt1].index

## SNIPPET 7.3 CROSS-VALIDATION CLASS WHEN OBSERVATIONS OVERLAP
class PurgedKFold(_BaseKFold): 
    Extend KFold class to work with labels that span intervals 
    The train is purged of observations overlapping test-label intervals 
    Test set is assumed contiguous (shuffle=False), 
    w/o training samples in between
def __init__(self,n_splits=3,t1=None,pctEmbargo=0.): 
    if not isinstance(t1,pd.Series): 
        raise ValueError('Label Through Dates must be a pd.Series')
    super(PurgedKFold,self).__init__(n_splits,shufﬂe=False,random_state=None)
    self.t1         = t1 
    self.pctEmbargo = pctEmbargo
def split(self,X,y=None,groups=None): 
    if (X.index==self.t1.index).sum()!=len(self.t1): 
        raise ValueError('X and ThruDateValues must have the same index') 
    indices     = np.arange(X.shape[0]) 
    mbrg        = int(X.shape[0]*self.pctEmbargo) 
    test_starts = [(i[0],i[-1]+1) for i in \ 
        np.array_split(np.arange(X.shape[0]),self.n_splits)] 
    for i,j in test_starts: 
        t0            = self.t1.index[i] # start of test set 
        test_indices  = indices[i:j] 
        maxT1Idx      = self.t1.index.searchsorted(self.t1[test_indices].max()) 
        train_indices = self.t1.index.searchsorted(self.t1[self.t1<=t0].index) 
        if maxT1Idx<X.shape[0]: # right train (with embargo) 
            train_indices=np.concatenate((train_indices,indices[maxT1Idx+mbrg:])) 
        yield train_indices,test_indices

## SNIPPET 7.4 USING THE PurgedKFold CLASS 
def cvScore(clf, X, y, sample_weight, scoring='neg_log_loss', t1=None, \
        cv=None, cvGen=None, pctEmbargo=None): 
    if scoring not in ['neg_log_loss','accuracy']: 
        raise Exception('wrong scoring method.') 
    from sklearn.metrics import log_loss,accuracy_score 
    from clfSequential import PurgedKFold 
    if cvGen is None: 
        cvGen=PurgedKFold(n_splits=cv,t1=t1,pctEmbargo=pctEmbargo) # purged 
    score=[] 
    for train,test in cvGen.split(X=X): 
        ﬁt=clf.ﬁt(X=X.iloc[train,:],y=y.iloc[train], \
            sample_weight=sample_weight.iloc[train].values) 
        if scoring=='neg_log_loss': 
            prob   = ﬁt.predict_proba(X.iloc[test,:]) 
            score_ = -log_loss(y.iloc[test],prob, \
                sample_weight=sample_weight.iloc[test].values,labels=clf.classes_) 
        else: 
            pred   = ﬁt.predict(X.iloc[test,:]) 
            score_ = accuracy_score(y.iloc[test],pred,sample_weight= \ 
                sample_weight.iloc[test].values) 
        score.append(score_) 
    return np.array(score)


